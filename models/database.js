var pg = require('pg');
var connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/node_api';

var client = new pg.Client(connectionString);
client.connect();
var query = client.query('CREATE TABLE things(id SERIAL PRIMARY KEY, name VARCHAR(255) not null)');
query.on('end', function() { client.end(); });
