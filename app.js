var express = require('express');
var router = express.Router();
var pg = require('pg');
var conString = process.env.DATABASE_URL || 'postgres://localhost:5432/node_api';

var app = express();
var client = new pg.Client(conString);
client.connect(function(err) {
  if(err) {
    console.error('could not connect to postgres', err);
  }
});

app.get('/api/things/', function (req, res) {
  var results = [];

  var query = client.query("SELECT * FROM things;");
  query.on('row', function(row) {
    results.push(row);
  });

  query.on('end', function() {
    client.end();
    return res.json(results);
  });
});

var server = app.listen(3000, function () {

  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);

});
